﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WeatherDesktopApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void inputCity_TextChanged(object sender, EventArgs e)
        {

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            // 'Details' object which contain all the weather data and sunrise/sunset times
            WeatherDetails details = new WeatherDetails(inputCity.Text);

            this.visibleCity.Text = details.city;
            this.currentTemperatureVal.Text = details.temperature + "°C";
            this.descriptionValue.Text = details.description;
            this.pressureVal.Text = details.pressure;
            this.humidityVal.Text = details.humidity;
            this.windSpeedVal.Text = details.windSpeed;
            this.sunriseVal.Text = details.sunriseTime;
            this.sunsetVal.Text = details.sunsetTime;

            this.weatherIcon.ImageLocation = @"..\..\images\" + details.weatherIcon + ".png";

            CountryData countryData = new CountryData(inputCity.Text);

            // We are using WebBrowser for the flag because it will support .svg format
            this.flagContainer.DocumentText = 
            "<html><head> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/></head><body>" +
            "<img src=\"" + countryData.flagUrl + "\" style=\"width: 100%\">" +
            "</body></html>";

            // Web Browser module will accept any html code in form of a string. The following code will inject an <iframe> containing
            // our map
            this.webBrowser1.DocumentText =
            "<html><head> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /></head><body>" +
            "<iframe width=\"400\" height=\"286\" frameborder=\"0\" style=\"border: 0\"" +
            "src=\"https://www.google.com/maps/embed/v1/place?key=AIzaSyCbXY73_OlgXUteMu1cp1yuwjNrsywtvFY&q=" + this.inputCity.Text + "\"" +
            "allowfullscreen> " +
            "</body></html>";

        }

        private void enterCity_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void descriptionValue_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_2(object sender, EventArgs e)
        {

        }

        private void label1_Click_3(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
        }

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void weatherIcon_Click(object sender, EventArgs e)
        {

        }
    }

    public class WeatherDetails
    {
        // client is used for retreving the HTTP data
        dynamic client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate });

        // Declaration on the needed variables
        public string temperature { get; set; }
        public string description { get; set; }
        public string pressure { get; set; }
        public string humidity { get; set; }
        public string windSpeed { get; set; }
        public string sunriseTime { get; set; }
        public string sunsetTime { get; set; }
        public string weatherIcon { get; set; }
        public string city { get; set; }

        public WeatherDetails(string _city)
        {
            getWeatherData(_city);
        }

        public void getWeatherData(string _city)
        {
            // Private API key we use for the weather forecast provider
            string apiKey = "d8ab77870812de67277ae47d3e9bf83e";

            // Object which returns coordinates based on the passed city name
            PlaceToCoordinates place = new PlaceToCoordinates(_city);

            // The following values are coming from Google API
            long lat = place.lat;
            long lng = place.lng;
            this.city = place.cityDetails;

            // Make GET call to the API destination:
            HttpResponseMessage response = client.GetAsync("https://api.darksky.net/forecast/" + apiKey + "/" + lat + "," + lng + "?units=si").Result;
            response.EnsureSuccessStatusCode();

            // Put the retrieved data inside 'result' string
            string result = response.Content.ReadAsStringAsync().Result;

            // Convert the retrieved string value into valid JSON object
            dynamic preparedData = JsonConvert.DeserializeObject(result);

            this.temperature = preparedData.currently.temperature;
            this.description = preparedData.hourly.summary;
            this.pressure = preparedData.currently.pressure;
            this.humidity = preparedData.currently.humidity;
            this.windSpeed = preparedData.currently.windSpeed;
            this.weatherIcon = preparedData.currently.icon;

            // The original SUNSET/SUNRISE times comes in Epoch format
            long originalSunriseTime = preparedData.daily.data[0].sunriseTime;
            long originalSunsetTime = preparedData.daily.data[0].sunsetTime;

            // Convert them into a valid c# DateTime object
            DateTimeOffset validSunriseTime = DateTimeOffset.FromUnixTimeSeconds(originalSunriseTime);
            this.sunriseTime = validSunriseTime.ToString("HH:mm");

            DateTimeOffset validSunsetTime = DateTimeOffset.FromUnixTimeSeconds(originalSunsetTime);
            this.sunsetTime = validSunsetTime.ToString("HH:mm");
        }
    }

    // Class returning coordinates and details of a given city or place
    public class PlaceToCoordinates
    {
        // client is used for retreving the HTTP data
        dynamic client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate });

        // Initialize Latitude and Longitude variables
        public long lat { get; set; }
        public long lng { get; set; }
        public string cityDetails { get; set; }

        public PlaceToCoordinates(string city)
        {
            ConvertPlaceToCoordinates(city);
        }

        public void ConvertPlaceToCoordinates(string city)
        {
            // Contact Google MAP api, and ask for coordinates of a given city or place. Works with my village too :)
            HttpResponseMessage response = client.GetAsync("http://maps.google.com/maps/api/geocode/json?address=" + city).Result;
            response.EnsureSuccessStatusCode();

            // Put the retrieved data inside 'result' string
            string result = response.Content.ReadAsStringAsync().Result;

            // Convert the retrieved string value into a valid JSON object
            dynamic preparedData = JsonConvert.DeserializeObject(result);
            this.lat = preparedData.results[0].geometry.location.lat;
            this.lng = preparedData.results[0].geometry.location.lng;
            this.cityDetails = preparedData.results[0].formatted_address;
        }
    }

    // Class returning selected country data. We need flag from this one
    public class CountryData
    {
        // client is used for retreving the HTTP data
        dynamic client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate });

        // Initialize flagUrl;
        public string flagUrl { get; set; }

        public CountryData(string _cityName)
        {
            PlaceToCoordinates place = new PlaceToCoordinates(_cityName);
            string onlyName = place.cityDetails.Split(',').Last();
            getCountryData(onlyName.Trim());
        }

        public void getCountryData(string _countryName)
        {
            Debug.WriteLine("Entered country: " +_countryName);
            // Make get request to a country details API provider
            string url = "https://restcountries.eu/rest/v2/name/" + _countryName + "?fullText=true";
            HttpResponseMessage response = client.GetAsync(url).Result;
            response.EnsureSuccessStatusCode();

            // Put the retrieved data inside 'result' string
            string result = response.Content.ReadAsStringAsync().Result;

            // Convert the retrieved string value into a valid JSON object
            dynamic preparedData = JsonConvert.DeserializeObject(result);
            this.flagUrl = preparedData[0].flag;
        }
    }

}
