﻿namespace WeatherDesktopApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.inputCity = new System.Windows.Forms.TextBox();
            this.enterCity = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            this.wrapperBox = new System.Windows.Forms.ListView();
            this.currentTemperatureKey = new System.Windows.Forms.Label();
            this.currentTemperatureVal = new System.Windows.Forms.Label();
            this.descriptionKey = new System.Windows.Forms.Label();
            this.descriptionValue = new System.Windows.Forms.Label();
            this.pressureKey = new System.Windows.Forms.Label();
            this.pressureVal = new System.Windows.Forms.Label();
            this.humidityKey = new System.Windows.Forms.Label();
            this.humidityVal = new System.Windows.Forms.Label();
            this.windKey = new System.Windows.Forms.Label();
            this.windSpeedVal = new System.Windows.Forms.Label();
            this.sunriseKey = new System.Windows.Forms.Label();
            this.sunriseVal = new System.Windows.Forms.Label();
            this.sunsetKey = new System.Windows.Forms.Label();
            this.sunsetVal = new System.Windows.Forms.Label();
            this.visibleCity = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.flagContainer = new System.Windows.Forms.WebBrowser();
            this.weatherIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.weatherIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.title.Location = new System.Drawing.Point(13, 13);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(188, 25);
            this.title.TabIndex = 0;
            this.title.Text = "Weather Application";
            this.title.Click += new System.EventHandler(this.label1_Click);
            // 
            // inputCity
            // 
            this.inputCity.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputCity.ForeColor = System.Drawing.Color.Gray;
            this.inputCity.Location = new System.Drawing.Point(18, 88);
            this.inputCity.Name = "inputCity";
            this.inputCity.Size = new System.Drawing.Size(457, 30);
            this.inputCity.TabIndex = 1;
            this.inputCity.TextChanged += new System.EventHandler(this.inputCity_TextChanged);
            // 
            // enterCity
            // 
            this.enterCity.AutoSize = true;
            this.enterCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enterCity.Location = new System.Drawing.Point(15, 67);
            this.enterCity.Name = "enterCity";
            this.enterCity.Size = new System.Drawing.Size(78, 18);
            this.enterCity.TabIndex = 2;
            this.enterCity.Text = "City name:";
            this.enterCity.Click += new System.EventHandler(this.enterCity_Click);
            // 
            // submitButton
            // 
            this.submitButton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.submitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.ForeColor = System.Drawing.Color.White;
            this.submitButton.Location = new System.Drawing.Point(481, 87);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(77, 31);
            this.submitButton.TabIndex = 3;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = false;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // wrapperBox
            // 
            this.wrapperBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wrapperBox.Location = new System.Drawing.Point(225, 135);
            this.wrapperBox.Name = "wrapperBox";
            this.wrapperBox.Size = new System.Drawing.Size(333, 238);
            this.wrapperBox.TabIndex = 4;
            this.wrapperBox.UseCompatibleStateImageBehavior = false;
            // 
            // currentTemperatureKey
            // 
            this.currentTemperatureKey.AutoSize = true;
            this.currentTemperatureKey.BackColor = System.Drawing.Color.White;
            this.currentTemperatureKey.Location = new System.Drawing.Point(237, 149);
            this.currentTemperatureKey.Name = "currentTemperatureKey";
            this.currentTemperatureKey.Size = new System.Drawing.Size(107, 13);
            this.currentTemperatureKey.TabIndex = 5;
            this.currentTemperatureKey.Text = "Current Temperature:";
            this.currentTemperatureKey.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // currentTemperatureVal
            // 
            this.currentTemperatureVal.AutoSize = true;
            this.currentTemperatureVal.Location = new System.Drawing.Point(348, 149);
            this.currentTemperatureVal.Name = "currentTemperatureVal";
            this.currentTemperatureVal.Size = new System.Drawing.Size(10, 13);
            this.currentTemperatureVal.TabIndex = 6;
            this.currentTemperatureVal.Text = "-";
            // 
            // descriptionKey
            // 
            this.descriptionKey.AutoSize = true;
            this.descriptionKey.BackColor = System.Drawing.Color.White;
            this.descriptionKey.Location = new System.Drawing.Point(237, 174);
            this.descriptionKey.Name = "descriptionKey";
            this.descriptionKey.Size = new System.Drawing.Size(63, 13);
            this.descriptionKey.TabIndex = 7;
            this.descriptionKey.Text = "Description:";
            // 
            // descriptionValue
            // 
            this.descriptionValue.Location = new System.Drawing.Point(348, 174);
            this.descriptionValue.Name = "descriptionValue";
            this.descriptionValue.Size = new System.Drawing.Size(186, 39);
            this.descriptionValue.TabIndex = 10;
            this.descriptionValue.Text = "-";
            this.descriptionValue.Click += new System.EventHandler(this.descriptionValue_Click);
            // 
            // pressureKey
            // 
            this.pressureKey.AutoSize = true;
            this.pressureKey.BackColor = System.Drawing.Color.White;
            this.pressureKey.Location = new System.Drawing.Point(237, 227);
            this.pressureKey.Name = "pressureKey";
            this.pressureKey.Size = new System.Drawing.Size(51, 13);
            this.pressureKey.TabIndex = 11;
            this.pressureKey.Text = "Pressure:";
            // 
            // pressureVal
            // 
            this.pressureVal.AutoSize = true;
            this.pressureVal.Location = new System.Drawing.Point(348, 227);
            this.pressureVal.Name = "pressureVal";
            this.pressureVal.Size = new System.Drawing.Size(10, 13);
            this.pressureVal.TabIndex = 12;
            this.pressureVal.Text = "-";
            // 
            // humidityKey
            // 
            this.humidityKey.AutoSize = true;
            this.humidityKey.BackColor = System.Drawing.Color.White;
            this.humidityKey.Location = new System.Drawing.Point(237, 255);
            this.humidityKey.Name = "humidityKey";
            this.humidityKey.Size = new System.Drawing.Size(50, 13);
            this.humidityKey.TabIndex = 13;
            this.humidityKey.Text = "Humidity:";
            // 
            // humidityVal
            // 
            this.humidityVal.AutoSize = true;
            this.humidityVal.Location = new System.Drawing.Point(348, 255);
            this.humidityVal.Name = "humidityVal";
            this.humidityVal.Size = new System.Drawing.Size(10, 13);
            this.humidityVal.TabIndex = 14;
            this.humidityVal.Text = "-";
            this.humidityVal.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // windKey
            // 
            this.windKey.AutoSize = true;
            this.windKey.BackColor = System.Drawing.Color.White;
            this.windKey.Location = new System.Drawing.Point(237, 283);
            this.windKey.Name = "windKey";
            this.windKey.Size = new System.Drawing.Size(69, 13);
            this.windKey.TabIndex = 15;
            this.windKey.Text = "Wind Speed:";
            // 
            // windSpeedVal
            // 
            this.windSpeedVal.AutoSize = true;
            this.windSpeedVal.Location = new System.Drawing.Point(348, 283);
            this.windSpeedVal.Name = "windSpeedVal";
            this.windSpeedVal.Size = new System.Drawing.Size(10, 13);
            this.windSpeedVal.TabIndex = 16;
            this.windSpeedVal.Text = "-";
            // 
            // sunriseKey
            // 
            this.sunriseKey.AutoSize = true;
            this.sunriseKey.BackColor = System.Drawing.Color.White;
            this.sunriseKey.Location = new System.Drawing.Point(237, 312);
            this.sunriseKey.Name = "sunriseKey";
            this.sunriseKey.Size = new System.Drawing.Size(71, 13);
            this.sunriseKey.TabIndex = 17;
            this.sunriseKey.Text = "Sunrise Time:";
            // 
            // sunriseVal
            // 
            this.sunriseVal.AutoSize = true;
            this.sunriseVal.Location = new System.Drawing.Point(348, 312);
            this.sunriseVal.Name = "sunriseVal";
            this.sunriseVal.Size = new System.Drawing.Size(10, 13);
            this.sunriseVal.TabIndex = 18;
            this.sunriseVal.Text = "-";
            // 
            // sunsetKey
            // 
            this.sunsetKey.AutoSize = true;
            this.sunsetKey.BackColor = System.Drawing.Color.White;
            this.sunsetKey.Location = new System.Drawing.Point(237, 339);
            this.sunsetKey.Name = "sunsetKey";
            this.sunsetKey.Size = new System.Drawing.Size(69, 13);
            this.sunsetKey.TabIndex = 19;
            this.sunsetKey.Text = "Sunset Time:";
            // 
            // sunsetVal
            // 
            this.sunsetVal.AutoSize = true;
            this.sunsetVal.Location = new System.Drawing.Point(348, 339);
            this.sunsetVal.Name = "sunsetVal";
            this.sunsetVal.Size = new System.Drawing.Size(10, 13);
            this.sunsetVal.TabIndex = 20;
            this.sunsetVal.Text = "-";
            this.sunsetVal.Click += new System.EventHandler(this.label1_Click_3);
            // 
            // visibleCity
            // 
            this.visibleCity.AutoSize = true;
            this.visibleCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.visibleCity.ForeColor = System.Drawing.SystemColors.Highlight;
            this.visibleCity.Location = new System.Drawing.Point(18, 135);
            this.visibleCity.MaximumSize = new System.Drawing.Size(200, 0);
            this.visibleCity.Name = "visibleCity";
            this.visibleCity.Size = new System.Drawing.Size(14, 20);
            this.visibleCity.TabIndex = 22;
            this.visibleCity.Text = "-";
            this.visibleCity.Click += new System.EventHandler(this.label2_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(588, 135);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(452, 286);
            this.webBrowser1.TabIndex = 23;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // flagContainer
            // 
            this.flagContainer.Location = new System.Drawing.Point(588, 52);
            this.flagContainer.MinimumSize = new System.Drawing.Size(20, 20);
            this.flagContainer.Name = "flagContainer";
            this.flagContainer.ScrollBarsEnabled = false;
            this.flagContainer.Size = new System.Drawing.Size(128, 66);
            this.flagContainer.TabIndex = 25;
            this.flagContainer.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser2_DocumentCompleted);
            // 
            // weatherIcon
            // 
            this.weatherIcon.Location = new System.Drawing.Point(18, 200);
            this.weatherIcon.Name = "weatherIcon";
            this.weatherIcon.Size = new System.Drawing.Size(197, 125);
            this.weatherIcon.TabIndex = 26;
            this.weatherIcon.TabStop = false;
            this.weatherIcon.Click += new System.EventHandler(this.weatherIcon_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 525);
            this.Controls.Add(this.weatherIcon);
            this.Controls.Add(this.flagContainer);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.visibleCity);
            this.Controls.Add(this.sunsetVal);
            this.Controls.Add(this.sunsetKey);
            this.Controls.Add(this.sunriseVal);
            this.Controls.Add(this.sunriseKey);
            this.Controls.Add(this.windSpeedVal);
            this.Controls.Add(this.windKey);
            this.Controls.Add(this.humidityVal);
            this.Controls.Add(this.humidityKey);
            this.Controls.Add(this.pressureVal);
            this.Controls.Add(this.pressureKey);
            this.Controls.Add(this.descriptionValue);
            this.Controls.Add(this.descriptionKey);
            this.Controls.Add(this.currentTemperatureVal);
            this.Controls.Add(this.currentTemperatureKey);
            this.Controls.Add(this.wrapperBox);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.enterCity);
            this.Controls.Add(this.inputCity);
            this.Controls.Add(this.title);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.weatherIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TextBox inputCity;
        private System.Windows.Forms.Label enterCity;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.ListView wrapperBox;
        private System.Windows.Forms.Label currentTemperatureKey;
        private System.Windows.Forms.Label currentTemperatureVal;
        private System.Windows.Forms.Label descriptionKey;
        private System.Windows.Forms.Label descriptionValue;
        private System.Windows.Forms.Label pressureKey;
        private System.Windows.Forms.Label pressureVal;
        private System.Windows.Forms.Label humidityKey;
        private System.Windows.Forms.Label humidityVal;
        private System.Windows.Forms.Label windKey;
        private System.Windows.Forms.Label windSpeedVal;
        private System.Windows.Forms.Label sunriseKey;
        private System.Windows.Forms.Label sunriseVal;
        private System.Windows.Forms.Label sunsetKey;
        private System.Windows.Forms.Label sunsetVal;
        private System.Windows.Forms.Label visibleCity;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.WebBrowser flagContainer;
        private System.Windows.Forms.PictureBox weatherIcon;
    }
}

